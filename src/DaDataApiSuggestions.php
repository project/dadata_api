<?php

namespace Drupal\dadata_api;

use Drupal\Component\Serialization\Json;

/**
 * Provides suggestions API.
 */
class DaDataApiSuggestions extends DaDataApiBase {

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  protected function getBaseUrl() {
    return 'https://suggestions.dadata.ru/suggestions/api/4_1/rs';
  }

  /**
   * Finds the nearest addresses by geographic coords.
   *
   * @param array $data
   *   The request parameters.
   * @param string $type
   *   Type of address to search ('address' or 'postal_unit').
   * @param string $method
   *   The request method ('GET' or 'POST'). Defaults to 'GET'.
   *
   * @return array|null
   *   Found addresses or NULL on failure.
   */
  public function geoLocate(array $data, $type, $method = 'GET') {
    if ($method === 'POST') {
      $options = [
        'method' => $method,
        'body' => Json::encode($data),
      ];
    }
    else {
      $options = [
        'query' => $data,
      ];
    }

    $url = $this->getRequestUrl("geolocate/$type");
    return $this->sendRequest($url, $options);
  }

  /**
   * Finds the location by specified IP address.
   *
   * @param string|null $ip
   *   IP address to detect location.
   *
   * @return array|null
   *   Detected location or NULL on failure.
   */
  public function ipLocate($ip = NULL) {
    static $result = [];

    if ($ip === NULL) {
      $ip = $this->getRequest()->getClientIp();
    }

    if (!isset($result[$ip])) {
      $url = $this->getRequestUrl('iplocate/address');
      $result[$ip] = $this->sendRequest($url, [
        'query' => ['ip' => $ip],
      ]);
    }
    return $result[$ip];
  }

  /**
   * Finds various information by specified parameters.
   *
   * @param array $data
   *   The request parameters.
   * @param string $type
   *   Type of information to search.
   *
   * @return array|null
   *   Found information or NULL on failure.
   */
  public function findById(array $data, $type) {
    $options = [
      'method' => 'POST',
      'body' => Json::encode($data),
    ];

    $url = $this->getRequestUrl("findById/$type");
    return $this->sendRequest($url, $options);
  }

  /**
   * Suggests various information by specified parameters.
   *
   * @param array $data
   *   The request parameters.
   * @param string $type
   *   Type of information to suggest.
   *
   * @return array|null
   *   Suggested information or NULL on failure.
   */
  public function suggest(array $data, $type) {
    $options = [
      'method' => 'POST',
      'body' => Json::encode($data),
    ];

    $url = $this->getRequestUrl("suggest/$type");
    return $this->sendRequest($url, $options);
  }

  /**
   * Gets the currently active request object.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The currently active request object.
   */
  protected function getRequest() {
    if (!isset($this->request)) {
      $this->request = \Drupal::request();
    }
    return $this->request;
  }

}
