<?php

namespace Drupal\dadata_api;

/**
 * Provides API with information methods.
 */
class DaDataApiInfo extends DaDataApiBase {

  /**
   * {@inheritdoc}
   */
  protected function getBaseUrl() {
    return 'https://dadata.ru/api/v2';
  }

  /**
   * Gets the information about API version.
   *
   * @return array|null
   *   The information about API version or NULL on failure.
   */
  public function getVersion() {
    static $result;

    if (!isset($result)) {
      $url = $this->getRequestUrl('version');
      $result = $this->sendRequest($url);
    }
    return $result;
  }

  /**
   * Gets the current balance.
   *
   * @return array|null
   *   The current balance or NULL on failure.
   */
  public function getBalance() {
    static $result;

    if (!isset($result)) {
      $url = $this->getRequestUrl('profile/balance');
      $result = $this->sendRequest($url, ['secret' => TRUE]);
    }
    return $result;
  }

  /**
   * Gets the usage statistics for the specified day.
   *
   * @param string $date
   *   The date (in format 'Y-m-d') to return statistics. The default is today.
   *
   * @return array|null
   *   The usage statistics or NULL on failure.
   */
  public function getStat($date = '') {
    static $result = [];

    if (!isset($result[$date])) {
      $url = $this->getRequestUrl('stat/daily');
      $result[$date] = $this->sendRequest($url, [
        'query' => ['date' => $date],
        'secret' => TRUE,
      ]);
    }
    return $result[$date];
  }

}
