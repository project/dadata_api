<?php

namespace Drupal\dadata_api\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Provides a form to manage settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dadata_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dadata_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dadata_api.settings');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $config->get('api_key'),
      '#size' => 40,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#description' => $this->t('Secret key for cleaner API.'),
      '#default_value' => $config->get('secret'),
      '#size' => 40,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Float representing the maximum number of seconds the API request may take. Use 0 to wait indefinitely.'),
      '#default_value' => $config->get('timeout'),
      '#min' => 0,
      '#max' => 30,
      '#step' => 0.01,
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('dadata_api.settings');

    // Save data in configuration.
    $config->set('api_key', $form_state->getValue('api_key'));
    $config->set('secret', $form_state->getValue('secret'));
    $config->set('timeout', $form_state->getValue('timeout'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
