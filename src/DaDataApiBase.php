<?php

namespace Drupal\dadata_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;

/**
 * Base class for API implementations.
 */
abstract class DaDataApiBase {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The key to access the API.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Secret key for cleaner API.
   *
   * @var string
   */
  protected $secret;

  /**
   * The API request timeout.
   *
   * @var float
   */
  protected $timeout;

  /**
   * DaDataApiBase constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Client $http_client) {
    $config = $config_factory->get('dadata_api.settings');
    $this->httpClient = $http_client;

    $this->setApiKey($config->get('api_key'));
    $this->setSecret($config->get('secret'));
    $this->setTimeout($config->get('timeout'));
  }

  /**
   * Sets the key to access the API.
   *
   * @param string $api_key
   *   The key to access the API.
   */
  public function setApiKey($api_key) {
    $this->apiKey = (string) $api_key;
  }

  /**
   * Sets the secret key for cleaner API.
   *
   * @param string $secret
   *   Secret key for cleaner API.
   */
  public function setSecret($secret) {
    $this->secret = (string) $secret;
  }

  /**
   * Sets the API request timeout.
   *
   * @param float $timeout
   *   The API request timeout.
   */
  public function setTimeout($timeout) {
    $this->timeout = (float) $timeout;
  }

  /**
   * Gets the base URL of the API.
   *
   * @return string
   *   The base URL of the API.
   */
  abstract protected function getBaseUrl();

  /**
   * Gets the URL of the specified request.
   *
   * @param string|null $path
   *   Request path relative to base URL.
   *
   * @return string
   *   The URL of the specified request.
   */
  protected function getRequestUrl($path = NULL) {
    $url = $this->getBaseUrl();

    if (!empty($path)) {
      $url .= '/' . $path;
    }
    return $url;
  }

  /**
   * Gets the default request headers.
   *
   * @return array
   *   The default request headers as name/value pairs.
   */
  protected function getRequestHeaders() {
    return [
      'Accept' => 'application/json',
      'Authorization' => 'Token ' . $this->apiKey,
    ];
  }

  /**
   * Sends a request to the API.
   *
   * @param string $url
   *   The URL of the request.
   * @param array $options
   *   Request options to apply. They are fully consistent with the GuzzleHttp
   *   request options. The following additional options are allowed:
   *   - method: HTTP method ('GET' or 'POST'). Defaults to 'GET'.
   *   - secret: Defaults to FALSE. Whether to add a header with a secret key.
   *
   * @return mixed|null
   *   Response data or NULL on failure.
   *
   * @see \GuzzleHttp\Client
   * @see \GuzzleHttp\RequestOptions
   */
  protected function sendRequest($url, array $options = []) {
    // Merge the default options.
    $options += [
      'headers' => [],
      'method' => 'GET',
      'secret' => FALSE,
      'timeout' => $this->timeout,
      'http_errors' => FALSE,
    ];

    // Merge the default headers.
    $options['headers'] += $this->getRequestHeaders();

    // Add header with secret if necessary.
    if ($options['secret']) {
      $options['headers'] += [
        'X-Secret' => $this->secret,
      ];
    }

    // Add header for POST request.
    if ($options['method'] === 'POST') {
      $options['headers'] += [
        'Content-Type' => 'application/json',
      ];
    }
    // Only GET and POST requests are allowed.
    elseif ($options['method'] !== 'GET') {
      $options['method'] = 'GET';
    }

    $response = $this->httpClient->request($options['method'], $url, $options);
    $status_code = $response->getStatusCode();

    if ($status_code >= 200 && $status_code < 300) {
      $data = $response->getBody()->getContents();
      $data = Json::decode($data);

      if (json_last_error() === JSON_ERROR_NONE) {
        return $data;
      }
    }
    return NULL;
  }

}
